--[[
	Copyright (c) 2017 Dale James

]]

require "core"

log("Session started.", log.category.notice)

--load classes
for _, fileName in ipairs(love.filesystem.getDirectoryItems("class")) do
	if fileName:match("^.+%.lua$") then
		log(string.format("Requiring file class/%s", fileName), log.category.notice)
		require("class." .. fileName:match("^(.+)%.lua$"))
	end
end

