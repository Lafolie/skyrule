--[[
    Copyright (c) 2017 Dale James

    Queue Object
    Simple circular queue operations for tables.

    usage:
        myQueue = queue()
        queue.add(x)
        y = stack.remove()

]]

local defaultQueueSize = 16

local queue = 
{
    __index = queue,

    __newindex = function()
        --restrict table to queue allocation
    end
}

local add = function(self, value)
    if self.count < self.size then
        rawset(self, self.tail, value)
        self.tail = (self.tail + 1) % self.size
        self.count = self.count + 1
    end
end

local remove = function(self)
    if self.count <= 0 then return end

    self.count = self.count - 1
    local value = self[self.head]
    self.head = (self.head + 1) % self.size
    return value 
end

local peek = function(self)
    return self[self.head]

end

local isEmpty = function(self)
    return self.count == 0
end

local isFull = function(self)
    return self.count == self.size
end

local newQueue = function(_, size)
    return setmetatable({head = 0, tail = 0, count = 0, add = add, remove = remove, peek = peek, isEmpty, isFull = isFull, size = size or defaultQueueSize}, queue)
end

return setmetatable({}, {__call = newQueue})