--[[
	Copyright (c) 2017 Dale James

	Stack Object
	Simple stack operations for tables.
	For perfrmance critical operations use table.insert() and table.remove() instead.

	usage:
		myStack = stack()
		stack.push(x)
		y = stack.pop()

]]

local stack = 
{
	__index = stack,

	__newindex = function()
		--restrict table to stack allocation
	end,
}

local push = function(self, value)
	local index = #self + 1
	if(index <= self.size or self.size == 0) then
		rawset(self, index, value)
	end
end

local pop = function(self)
	if(#self >= 0) then
		local result = rawget(self, #self)
		rawset(self, #self, nil)
		return result
	end
end

local peek = function(self)
	if(#self >= 0) then
		return self[#self]
	end
end

local isEmpty = function(self)
	return #self == 0
end

local isFull = function(self)
	return #self <= self.size
end

local newStack = function(_, size)
	return setmetatable({push = push, pop = pop, peek = peek, isEmpty = isEmpty, isFull = isFull, size = size or 0}, stack)
end

return setmetatable({}, {__call = newStack})