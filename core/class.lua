--[[
	Copyright (c) 2017 Dale James

	Class Object
	Allows usage of OOP patterns. Supports singular inheritance.

	Tables initialised in class definitions and not in function scope are 'static' (shared by instances).
	For safety, confine non-function member declarations to function scope (e.g. in construct() )

	To access the parent class use _parent

	Usage:
		MyClass = class { myproperty = 10 }
		MyChildClass = class myClass { myOtherProperty = true }
		bool = MyChildClass:is(MyClass)

	Todo:
		Metaevent support

]]

--cascading property cloning function
local clone 
clone = function(object, result)
		local result = result or {}

		--clone local properties
		for k, v in pairs(object) do
			if not result[k] then result[k] = v end
		end

		--clone parent props
		if object._parent then clone(object._parent, result) end

		return result
	end

--metatable for class-derived objects
local classMeta = 
{
	--instantiate an object from this class
	__call = function(t, ...)
		local obj = clone(t)
		obj:construct(...)
		obj._parent = t._parent and t
		return obj
	end
}

local class = setmetatable(
{
	--class constructor (may be omitted)
	construct = function(self) end,

	--class type reflection
	is = function(self, other)
		return self == other or (self._parent and self._parent:is(other))
	end
}, 
{
	__call = function(_, parent)
		--check whether a parent was passed (new class definitions should not manually specify _parent!)
		if parent._parent then
			return function(tbl)
				tbl._parent = parent
				local tbl = clone(tbl._parent, tbl)
				return setmetatable(tbl, classMeta)
			end
		else
			local tbl = parent
			tbl._parent = class
			local tbl = clone(tbl._parent, tbl)
			return setmetatable(tbl, classMeta)
		end
	end
})

return class