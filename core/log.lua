--[[
	Copyright (c) 2017 Dale James

	Log Object
	Used for debugging purposes.

	usage:
		log "Hello World"
		log(myValue, log.category.warning)

]]

local logDir = "logs/"
local logBuffer = {}
local fileCount = #love.filesystem.getDirectoryItems(logDir) + 1 --used for generating filename
local fname = string.format("%s[%s]skyrule_%s.log", logDir, os.date("%X"):gsub("[:]", "-"), fileCount < 10 and "0" .. fileCount or fileCount)

local log =
{
	__index = log,

	__call = function(t, value, category, transient)
		category = category or log.category.debug

		local value = string.format("[%s] %s: %s", os.date("%X"), category, value)
		if not transient then 
			table.insert(logBuffer, value) 
		end
		print(value)
	end

}

local category =
{
	warning = "Warning",
	error = "Error",
	debug = "Debug",
	notice = "Notice"
}

dump = function()
	if not love.filesystem.exists("logs") then
		love.filesystem.createDirectory("logs")
	end

	local file, err = love.filesystem.newFile(fname, "w")
	local succ, err = file:write(table.concat(logBuffer, "\n"))
	if succ then
		file:flush()
		file:close()
	else
		log(err)
	end
end

--sneak into errhand
local errhand = love.errhand
function love.errhand(msg)
	errhand(msg)
	dump()
end

return setmetatable({category = category, dump = dump}, log)