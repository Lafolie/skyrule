--[[
	Copyright (c) 2017 Dale James

	Serialization Functions
	Recursive serializatoin of tables. Generates Lua code.

	usage:
		result = serialize(tbl)

]]

local validTypes = 
{
	boolean = true,
	number = true,
	string = true,
}

local generateLua --forward declaration for recursion
generateLua = function(tbl, tabs)
	local tabs = tabs or ""
	local output = {string.format("%s{", tabs)}
	tabs = tabs .. "\t"
	local kType, vType

	for k, v in pairs(tbl) do
		kType = type(k)
		vType = type(v)
		if validTypes[kType] and validTypes[vType] then
			--build string from kv
			table.insert(output, string.format(vType == "string" and "%s%s=%q," or "%s%s=%s,", tabs, tostring(k), tostring(v)))

		elseif validTypes[kType] and vType == "table" then
			--parse table
			table.insert(output, string.format("%s%s=", tabs, tostring(k)))
			table.insert(output, generateLua(v, tabs))
		end
	end

	tabs = tabs:sub(1, #tabs-1)
	return string.format("%s\n%s}", table.concat(output, "\n"), tabs)

end

return generateLua