--[[
	Copyright (c) 2017 Dale James

	Quad Tree
	Spacial partitiong object.

]]

require "class.rect"

--constants
local nodeCapacity = 1
local maxDepth = 5

local lookUp = setmetatable({}, {__mode = "v"})


Quadtree = class
{
	construct = function(self, bounds, depth, parent)
		self.bounds = bounds
		self.depth = depth or 0
		self.leaves = setmetatable({}, {__mode = "v"})
		self.objectCount = 0
		self.parent = parent
		
	end,

	insert = function(self, object)
		--check whether object lies within node
		if not self.bounds:collide(object) then return end

		--if there is free space, add as leaf
		if (#self.leaves < nodeCapacity and not self.northWest) or self.depth == maxDepth then
			table.insert(self.leaves, object)
			lookUp[object] = self
			return true
		end

		--not enough space, subdivide quadrant
		if not self.northWest then
			self:subdivide()
		end

		--attempt to store in child node
		if self.northWest:insert(object) or
		   self.northEast:insert(object) or
		   self.southWest:insert(object) or
		   self.southEast:insert(object) 
		then
			return true
		end

		--error
		return false
	end,

	remove = function(self, object)
		local node = lookUp[object]
		
		if not node then return end
		--assert(node, 'Attempt to remove object not in quadtree')

		--find and remove
		for k, obj in ipairs(node.leaves) do
			if obj == object then
				table.remove(node.leaves, k)
				lookUp[object] = nil
				break
			end
		end

		--see whether node can possibly be collapsed
		if node.parent and #node.leaves <= nodeCapacity then
			node.parent:collapse()
		end

		return object
	end,

	getNumLeaves = function(self)
		local count = #self.leaves

		if self.northWest then
			count = count +
				self.northWest:getNumLeaves() +
				self.northEast:getNumLeaves() +
				self.southWest:getNumLeaves() +
				self.southEast:getNumLeaves()
		end

		return count
	end,
	-- remove = function(self, object)
	-- 	--check if object lies within node
	-- 	if not self.bounds:collide(object) then return end
		
	-- 	if not self.northWest then
	-- 		for k, obj in ipairs(self.leaves) do
	-- 			if obj == object then
	-- 				table.remove(self.leaves, k)
	-- 				return
	-- 			end
	-- 		end
	-- 		return --false positive from collision check (when object crosses boundaries)
	-- 	end

	-- 	local _ = self.northWest:remove(object) or self.northEast:remove(object) or self.southWest:remove(object) or self.southEast:remove(object)
	-- end,

	subdivide = function(self)
		local x, y = self.bounds.x, self.bounds.y
		local w, h = self.bounds:getHalfDimensions()
		
		--create children
		self.northWest = Quadtree(Rect(x, y, w, h), self.depth + 1, self)
		self.northEast = Quadtree(Rect(x + w, y, w, h), self.depth + 1, self)
		self.southWest = Quadtree(Rect(x, y + h, w, h), self.depth + 1, self)
		self.southEast = Quadtree(Rect(x + w, y + h, w, h), self.depth + 1, self)

		--pass leaves on to children
		for _, obj in ipairs(self.leaves) do
			self:insert(obj)
		end

		self.leaves = setmetatable({}, {__mode = "v"})
	end,

	collapse = function(self)
		local count = self:getNumLeaves()

		if count > nodeCapacity then return end --exit if no need to collapse


		local newLeaves = setmetatable({}, {__mode = "v"})
		
		self.northWest:emptyInto(newLeaves)
		self.northEast:emptyInto(newLeaves)
		self.southWest:emptyInto(newLeaves)
		self.southEast:emptyInto(newLeaves)

		self.northWest = nil
		self.northEast = nil
		self.southWest = nil
		self.southEast = nil

		for _, obj in ipairs(newLeaves) do
			lookUp[obj] = self
		end
		self.leaves = newLeaves
		self.parent:collapse()
	end,

	emptyInto = function(self, tbl)
		--dump leaves into table
		for _, obj in ipairs(self.leaves) do
			table.insert(tbl, obj)
		end
	end,

	-- numObjects = function(self)
	-- 	local count = #self.leaves

	-- 	if self.northWest then
	-- 		count = count + 
	-- 			self.northWest:numObjects() + 
	-- 			self.northEast:numObjects() + 
	-- 			self.southWest:numObjects() +
	-- 			self.southEast:numObjects()
	-- 	end

	-- 	return count
	-- end,

	query = function(self, area, output)
		output = output or {}

		--return if no intersection
		if(not self.bounds:collide(area)) then
			return output
		end

		--append valid objects to output
		for _, object in ipairs(self.leaves) do
			if(object:collide(area)) then
				table.insert(output, object)
			end
		end
		
		--return if no children
		if not self.northWest then
			return output
		end

		--query children
		output = self.northWest:query(area, output)
		output = self.northEast:query(area, output)
		output = self.southWest:query(area, output)
		output = self.southEast:query(area, output)

		return output

	end
}