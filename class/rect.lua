--[[
	Copyright (c) 2017 Dale James

	Rect
	Simple rectangle class

	Usage:
		myRect = rect(x, y, width, height)
		bool = myRect:collide(otherRect)
		bool = myRect:collide(myVector)
]]

Rect = class
{
	construct = function(self, x, y, width, height)
		self.x = x or 0
		self.y = y or 0
		self.width = width or 1
		self.height = height or 1
	end,

	getLocation = function(self)
		return vector(self.x, self.y)
	end,

	setLocation = function(self, vector)
		self.x = vector.x
		self.y = vector.y
	end,

	addLocation = function(self, vector)
		self.x = self.x + vector.x
		self.y = self.y + vector.y
	end,

	getSize = function(self)
		return vector(self.width, self.height)
	end,

	getHalfDimensions = function(self)
		return self.width * 0.5, self.height * 0.5
	end,

	collide = function(self, other)
		if other.is and other:is(Rect) then
			--rect on rect
			return not(self.x + self.width < other.x or other.x + other.width < self.x or self.y + self.height < other.y or other.y + other.height < self.y)
		elseif vector.isvector(other) then
			return not(other.x < self.x or self.x + self.width < other.x or other.y < self.y or self.y + self.height < other.y)
		end
	end,

	unpack = function(self)
		return self.x, self.y, self.width, self.height
	end

	
}